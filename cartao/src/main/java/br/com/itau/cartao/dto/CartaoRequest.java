package br.com.itau.cartao.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CartaoRequest {

    @NotBlank(message = "O campo número do cartão não pode ser vazio")
    private String numero;

    @NotNull(message = "O campo de id do cliente não pode ser nulo")
    private Integer clienteId;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }
}
