package br.com.itau.cartao.mapper;

import br.com.itau.cartao.dto.CartaoRequest;
import br.com.itau.cartao.dto.CartaoResponse;
import br.com.itau.cartao.model.Cartao;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapper {

    public Cartao toCartao(CartaoRequest cartaoRequestDto) {
        Cartao cartao = new Cartao();
        cartao.setNumero(cartaoRequestDto.getNumero());
        cartao.setClienteId(cartaoRequestDto.getClienteId());
        return cartao;
    }

    public CartaoResponse toCartaoResponse(Cartao cartao) {
        CartaoResponse cartaoResponse = new CartaoResponse();
        cartaoResponse.setId(cartao.getId());
        cartaoResponse.setNumero(cartao.getNumero());
        cartaoResponse.setClienteId(cartao.getClienteId());
        cartaoResponse.setAtivo(cartao.isAtivo());
        return cartaoResponse;
    }
}
