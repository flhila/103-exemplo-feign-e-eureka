package br.com.itau.cartao.clients.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "O cliente especificado não foi encontrado")
public class ClienteNotFoundException extends RuntimeException {
}
