package br.com.itau.cartao.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Cartao não encontrado com o número informado")
public class CartaoNotFoundException extends RuntimeException {
}
