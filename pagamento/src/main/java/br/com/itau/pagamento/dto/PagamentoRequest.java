package br.com.itau.pagamento.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class PagamentoRequest {

    @NotNull(message = "Campo Id do Cartão é obrigatório")
    private Integer cartao_id;

    @NotBlank(message = "Campo descrição é obrigatório")
    private String descricao;

    @NotNull(message = "Campo valor é obrigatório")
    private Double valor;

    public Integer getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(Integer cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
