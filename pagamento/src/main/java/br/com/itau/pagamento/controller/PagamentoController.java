package br.com.itau.pagamento.controller;

import br.com.itau.pagamento.dto.PagamentoRequest;
import br.com.itau.pagamento.dto.PagamentoResponse;
import br.com.itau.pagamento.mapper.PagamentoMapper;
import br.com.itau.pagamento.model.Pagamento;
import br.com.itau.pagamento.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @Autowired
    private PagamentoMapper pagamentoMapper;

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public PagamentoResponse criarPagamento(@Valid @RequestBody PagamentoRequest pagamentoRequest) {
        Pagamento pagamento = pagamentoMapper.toPagamento(pagamentoRequest);
        pagamento = pagamentoService.criarPagamento(pagamento);
        PagamentoResponse pagamentoResponse = pagamentoMapper.toPagamentoResponse(pagamento) ;
        return pagamentoResponse;
    }

    @GetMapping("/{id_cartao}")
    public List<PagamentoResponse> listarPagamentosCartao(@PathVariable Integer id_cartao) {
        List<Pagamento> pagamentoList = pagamentoService.listarPorCartao(id_cartao);
        return pagamentoMapper.toPagamentoResponseList(pagamentoList);
    }
}
