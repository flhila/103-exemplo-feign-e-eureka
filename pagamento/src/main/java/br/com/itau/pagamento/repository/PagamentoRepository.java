package br.com.itau.pagamento.repository;

import br.com.itau.pagamento.model.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {

    public List<Pagamento> findAllByCartaoId(Integer cartaoId);
}
