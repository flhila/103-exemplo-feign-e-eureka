package br.com.itau.cartao.dto;

import javax.validation.constraints.NotNull;

public class Status {

    @NotNull(message = "O campo ativo não pode ser nulo")
    private Boolean ativo;

    public Boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
