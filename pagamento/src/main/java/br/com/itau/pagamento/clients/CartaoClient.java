package br.com.itau.pagamento.clients;

import br.com.itau.pagamento.clients.dto.Cartao;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cartao")
public interface CartaoClient {

    @GetMapping("/cartao/id/{id}")
    Cartao getCartaoById(@PathVariable Integer id);
}
