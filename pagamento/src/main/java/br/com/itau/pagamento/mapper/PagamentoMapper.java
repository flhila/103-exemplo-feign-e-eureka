package br.com.itau.pagamento.mapper;

import br.com.itau.pagamento.dto.PagamentoRequest;
import br.com.itau.pagamento.dto.PagamentoResponse;
import br.com.itau.pagamento.model.Pagamento;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PagamentoMapper {

    public Pagamento toPagamento(PagamentoRequest pagamentoRequest) {
        Pagamento pagamento = new Pagamento();
        pagamento.setCartaoId(pagamentoRequest.getCartao_id());
        pagamento.setDescricao(pagamentoRequest.getDescricao());
        pagamento.setValor(pagamentoRequest.getValor());
        return pagamento;
    }


    public PagamentoResponse toPagamentoResponse(Pagamento pagamento) {
        PagamentoResponse pagamentoResponse = new PagamentoResponse();
        pagamentoResponse.setId(pagamento.getId());
        pagamentoResponse.setCartao_id(pagamento.getCartaoId());
        pagamentoResponse.setDescricao(pagamento.getDescricao());
        pagamentoResponse.setValor(pagamento.getValor());
        return pagamentoResponse;
    }

    public List<PagamentoResponse> toPagamentoResponseList(List<Pagamento> pagamentoList) {
        List<PagamentoResponse> pagamentoResponseList = new ArrayList<PagamentoResponse>();
        for(Pagamento pagamento : pagamentoList) {
            PagamentoResponse pagamentoResponse = new PagamentoResponse();
            pagamentoResponse.setId(pagamento.getId());
            pagamentoResponse.setValor(pagamento.getValor());
            pagamentoResponse.setDescricao(pagamento.getDescricao());
            pagamentoResponse.setCartao_id(pagamento.getCartaoId());
            pagamentoResponseList.add(pagamentoResponse);
        }
        return pagamentoResponseList;
    }
}
