package br.com.itau.cartao.controller;

import br.com.itau.cartao.dto.CartaoRequest;
import br.com.itau.cartao.dto.CartaoResponse;
import br.com.itau.cartao.dto.Status;
import br.com.itau.cartao.mapper.CartaoMapper;
import br.com.itau.cartao.model.Cartao;
import br.com.itau.cartao.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import javax.validation.ValidationException;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private CartaoMapper cartaoMapper;

    @GetMapping("/id/{id}")
    public CartaoResponse buscarPorId(@PathVariable Integer id) {
        Cartao cartao =  cartaoService.buscarPorId(id);
        CartaoResponse cartaoResponse = cartaoMapper.toCartaoResponse(cartao);
        return cartaoResponse;
    }


    @GetMapping("/{numero}")
    public CartaoResponse buscarPorNumero(@PathVariable String numero) {
        System.out.println("Buscando cartão: " + numero);
        Cartao cartao = cartaoService.buscarPorNumero(numero);
        CartaoResponse cartaoResponse = cartaoMapper.toCartaoResponse(cartao);
        return cartaoResponse;
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public CartaoResponse criarCartao(@Valid @RequestBody CartaoRequest cartaoRequestDto) {
        System.out.println("Criando cartão: " + cartaoRequestDto.toString());
        Cartao cartao = cartaoMapper.toCartao(cartaoRequestDto);
        cartao = cartaoService.criarCartao(cartao);
        CartaoResponse cartaoResponse = cartaoMapper.toCartaoResponse(cartao);
        return cartaoResponse;
    }

    @PatchMapping("/{numero}")
    public Cartao alterarStatusCartao(@PathVariable String numero, @Valid @RequestBody Status status) {
        try {
            return cartaoService.alterarStatus(numero, status);
        } catch (ValidationException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
}
