package br.com.itau.pagamento.service;

import br.com.itau.pagamento.clients.CartaoClient;
import br.com.itau.pagamento.clients.exception.CartaoNotFoundException;
import br.com.itau.pagamento.model.Pagamento;
import br.com.itau.pagamento.repository.PagamentoRepository;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoClient cartaoClient;

    public Pagamento criarPagamento(Pagamento pagamento) {
        try {
            cartaoClient.getCartaoById(pagamento.getCartaoId());
        } catch (FeignException.FeignClientException.NotFound e) {
            throw new CartaoNotFoundException();
        }
        return pagamentoRepository.save(pagamento);
    }

    public List<Pagamento> listarPorCartao(Integer id_cartao) {
        try {
            cartaoClient.getCartaoById(id_cartao);
        } catch (FeignException.FeignClientException.NotFound e) {
            throw new CartaoNotFoundException();
        }
        return pagamentoRepository.findAllByCartaoId(id_cartao);
    }
}
