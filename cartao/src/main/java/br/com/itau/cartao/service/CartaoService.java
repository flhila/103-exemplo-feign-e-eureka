package br.com.itau.cartao.service;

import br.com.itau.cartao.clients.ClienteClient;
import br.com.itau.cartao.clients.exception.ClienteNotFoundException;
import br.com.itau.cartao.dto.Status;
import br.com.itau.cartao.exception.CartaoNotFoundException;
import br.com.itau.cartao.model.Cartao;
import br.com.itau.cartao.repository.CartaoRepository;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteClient clienteClient;

    public Cartao buscarPorId(Integer id) {
        Optional<Cartao> cartao = cartaoRepository.findById(id);
        if (cartao.isPresent()) {
            return cartao.get();
        } else {
            throw new CartaoNotFoundException();
        }
    }

    public Cartao buscarPorNumero(String numero) {
        Optional<Cartao> cartao = cartaoRepository.findByNumero(numero);
        if (cartao.isPresent()) {
            return cartao.get();
        } else {
            throw new CartaoNotFoundException();
        }
    }

    public Cartao criarCartao(Cartao cartao) {
        try {
            clienteClient.getClienteById(cartao.getClienteId());
        } catch (FeignException.FeignClientException.NotFound e) {
            throw new ClienteNotFoundException();
        }
        cartao.setAtivo(false);
        return cartaoRepository.save(cartao);
    }

    public Cartao alterarStatus(String numero, Status status) {
        Cartao cartao = buscarPorNumero(numero);
        if (cartao != null) {
            cartao.setAtivo(status.isAtivo());
            return cartaoRepository.save(cartao);
        } else {
            throw new CartaoNotFoundException();
        }
    }
}