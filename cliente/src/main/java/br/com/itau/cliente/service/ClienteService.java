package br.com.itau.cliente.service;

import br.com.itau.cliente.exception.ClienteNotFoundException;
import br.com.itau.cliente.model.Cliente;
import br.com.itau.cliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.ValidationException;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente buscarPorId(Integer id) {
        Optional<Cliente> cliente = clienteRepository.findById(id);
        if (cliente.isPresent()) {
            return clienteRepository.findById(id).get();
        } else {
            throw new ClienteNotFoundException();
        }
    }

    public Iterable<Cliente> listarClientes() {
        return clienteRepository.findAll();
    }

    public Cliente criarCliente(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

}
