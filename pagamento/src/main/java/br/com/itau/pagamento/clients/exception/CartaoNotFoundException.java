package br.com.itau.pagamento.clients.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "O cartão especificado não foi encontrado")
public class CartaoNotFoundException extends RuntimeException {
}
