package br.com.itau.cartao.repository;

import br.com.itau.cartao.model.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {

    public Optional<Cartao> findByNumero(String numero);
}
